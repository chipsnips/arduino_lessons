/*
Analog input and serial output example

Reads an analog input.  Sends response to serial port formatted for the 
Arduino Serial Plotter and Monitor.

 The circuit:
 * Inventem Light Sensor on A1

 created 8 Apr 2017
 by Jon Young, Inventem, Inc.

 This example code is in the public domain.
 */

// This constant won't change.  It is used to associate a meaningful name with the pin used.
const int analogInLightSensor = A1;  // Analog input pin that the light sensor is attached to

int lightSensorValue = 0;        // value light sensor analog input

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  // read the analog light sensor input value:
  lightSensorValue = analogRead(analogInLightSensor);

  // print the results to the serial plotter
  Serial.println(lightSensorValue);
  
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(2);
}
