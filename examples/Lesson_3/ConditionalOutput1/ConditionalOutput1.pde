/*
  Analog input, analog output, serial output

Reads analog input light sensor.  Sends response to serial port formatted for the 
Arduino Serial Monitor.

 The circuit:
 * Inventem Light Sensor on A1
 * Inventem LED on pin 5

 created 8 Apr 2017
 by Jon Young, Inventem, Inc.

 This example code is in the public domain.
 */

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInLightSensor = A1;  // Analog input pin that the light sensor is attached to
const int digitalOutLED1 = 5;
const int darkTripValue = 500;

int lightSensorValue = 0;        // value of light sensor analog input

void setup() {
  // initialize serial communications at 9600 bps
  Serial.begin(9600);
  
  // initialize output LED1
  pinMode(digitalOutLED1, OUTPUT);
}

void loop() {
  // read the analog in value:
  lightSensorValue = analogRead(analogInLightSensor);

  // print the results to the serial plotter
  Serial.println(lightSensorValue);

  if(lightSensorValue < darkTripValue){
    digitalWrite(digitalOutLED1, HIGH); // Turn on LED for "night light" effect
  }else{
    digitalWrite(digitalOutLED1, LOW);  // Turn off LED in bright light
  }
  
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(2);
}
