/*
  Analog input, digital output, serial output

Reads analog input light and force sensor.  Sends response to serial port formatted for the 
Arduino Serial Monitor.

LED on pin 5 will turn on when ambient light reading is less than force sensor reading.

 The circuit:
 * Inventem Light Sensor on A1
 * Inventem Force Sensor on A2
 * Inventem LED on pin 5

 created 8 Apr 2017
 by Jon Young

 This example code is in the public domain.

 */

// These constants won't change.  They're used to give meaningful names
// to the pins used:
const int analogInLightSensor = A1;  // Analog input pin that the light sensor is attached to
const int analogInForceSensor = A2;  // Analog input pin that the force sensor is attached to
const int digitalOutLED1 = 5;

int lightSensorValue = 0;        // value of light sensor analog input
int forceSensorValue = 0;        // value of force sensor analog input

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  
  // initialize output LED1
  pinMode(digitalOutLED1, OUTPUT);
}

void loop() {
  // read the analog in value:
  lightSensorValue = analogRead(analogInLightSensor);
  forceSensorValue = analogRead(analogInForceSensor);

  // print the results to the serial plotter
  Serial.println();               // End of line tells serial plotter the following values are a new value set
  Serial.print(lightSensorValue);
  Serial.print(" ");              // Space between values tells serial plotter these are seprate values to plot seperately.
  Serial.print(forceSensorValue);
  
  if(lightSensorValue < forceSensorValue){
    digitalWrite(digitalOutLED1, HIGH); // Turn on LED for "night light" effect
  }else{
    digitalWrite(digitalOutLED1, LOW);  // Turn off LED in bright light
  }
  
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(2);
}
